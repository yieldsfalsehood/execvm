#!/bin/bash

set -euo pipefail

execvm pack -o ">" "H" 1234 \
    | od -t x1 | cut -d' ' -f1 --complement -s | tr -d ' ' \
    | grep -q '^04d2$'

execvm pack "3s" iii \
    | od -t x1 | cut -d' ' -f1 --complement -s | tr -d ' ' \
    | grep -q '^696969$'

execvm pack "B" 1 "B" 2 \
    | od -t x1 | cut -d' ' -f1 --complement -s | tr -d ' ' \
    | grep -q '^0102$'
