#!/usr/bin/env python3

from execvm import executions


def execution(argv):
    return executions.ExecVP(argv + ["echo", "-n", "hello", "world"])
