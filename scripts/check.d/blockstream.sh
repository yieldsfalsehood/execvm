#!/bin/bash

set -euo pipefail

function pack() {
    # manually encode an entire stream, which is a data packet then an
    # eof packet, using block size 10
    execvm pack -o ">" \
           "c" 0 "H" 5 "7s" hello \
           "c" 1 "H" 0 "7s" ""
}

# check stream encoding
expected=$(pack | base64 -)
actual=$(echo -n hello | \
           execvm encode-stream 10 \
           | base64 -)
[ "$expected" = "$actual" ]

# check stream decoding
pack \
    | execvm decode-stream 10 \
    | grep -q '^hello$'

# check both ways
echo -n hello \
    | execvm encode-stream 10 \
    | execvm decode-stream 10 \
    | grep -q '^hello$'
