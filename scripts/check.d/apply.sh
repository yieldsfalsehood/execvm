#!/bin/bash

set -euo pipefail

suffix="${PYTHONPATH:+:}${PYTHONPATH:-}"
PYTHONPATH="scripts/check.d/${suffix}" execvm exec \
          -m hello docker run -it --rm alpine \
    | grep -q '^hello world$'

# suffix="${PYTHONPATH:+:}${PYTHONPATH:-}"
# PYTHONPATH="scripts/check.d/${suffix}" execvm exec \
#           -m hello execvm run \
#     | grep -q '^hello world$'
