#!/bin/bash

set -euo pipefail

execvm exec echo hello world \
    | grep -q "^hello world$"

execvm exec < scripts/check.d/hello-world.yaml \
    | grep -q '^hello world$'

execvm exec -c $(base64 -w0 scripts/check.d/hello-world.yaml) \
    | grep -q '^hello world$'

suffix="${PYTHONPATH:+:}${PYTHONPATH:-}"
PYTHONPATH="scripts/check.d/${suffix}" execvm exec \
          -m echo hello world \
    | grep -q '^hello world$'
