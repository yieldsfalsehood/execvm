FROM alpine:latest

RUN ["/sbin/apk", \
        "add", \
        "--no-cache", \
        "python3", \
        "py3-pip"]

ADD . /src

RUN ["/usr/bin/python3", "-m", "pip", "install", "/src"]

RUN ["rm", "-rf", "/src"]
