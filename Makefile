
SNAME = execvm

# https://stackoverflow.com/a/7367903
GUARD-%:
	@if [ "${${*}}" = "" ]; then \
	    echo "Environment variable $* not set"; \
	    exit 1; \
	fi

develop:
	@pip install -Ur requirements.txt

flake8:
	@python -m flake8 src

pylint:
	@PYTHONPATH=src python -m pylint src

.PHONY: dist
dist: GUARD-VERSION
	@python -m build --wheel --sdist .

docker:
	@git archive HEAD | gzip | docker build -t execvm -

install: GUARD-VERSION
	@pip install .

.PHONY: check
check:
	@find scripts/check.d \
	  -executable \
	  -type f \
	  -name '*.sh' \
	  -exec scripts/check '{}' \;
