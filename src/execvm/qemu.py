#!/usr/bin/env python3

import os
import itertools

from . import defaults
from . import vector
from . import yaml


def expand(flag, values):
    if values:
        return itertools.chain.from_iterable(
            (flag, value)
            for value in values
        )
    return []


class AlpineCommandLine(yaml.YAMLObject):

    yaml_tag = "!alpine-command-line"

    def __init__(self, console, root, execution,
                 init=None, debug=False):
        super().__init__()
        self.console = console
        self.root = root
        self.execution = execution
        self.init = init
        self.debug = debug

    def args(self):

        yield "oops=panic"
        yield "panic=-1"

        if self.debug:
            yield "debug"
        else:
            yield "quiet"

        yield f"root={self.root}"

        # we should mknod in the bootstrap to clone dev ttyS0 and dev
        # ttyS1 to well known names. that way we'd be able to use
        # those device names everywhere rather than have to treat them
        # like paths and take the basename here.
        basename = os.path.basename(self.console)
        yield f"console={basename}"

        if self.init:
            yield f"init={self.init}"

        encoded = str(self.execution)
        yield f"init_args={encoded}"

    def __str__(self):
        return " ".join(self.args())


class Qemu(vector.Vector):

    yaml_tag = "!qemu"

    def __init__(self, kernel, initrd, append,
                 drives=None,
                 cdroms=None,
                 args=None,
                 _qemu=defaults.QEMU):
        super().__init__()
        self.kernel = kernel
        self.initrd = initrd
        self.append = append
        self.drives = drives or []
        self.cdroms = cdroms or []
        self.args = args or []
        self._qemu = _qemu

    def vectorize(self):
        return [
            self._qemu,
            "-kernel", str(self.kernel),
            "-initrd", str(self.initrd),
            "-append", str(self.append),
            *expand("-drive", self.drives),
            *expand("-cdrom", self.cdroms),
            *self.args,
        ]

    @classmethod
    def to_yaml(cls, dumper, self):
        return dumper.represent_mapping(
            cls.yaml_tag,
            {
                "kernel": str(self.kernel),
                "initrd": str(self.initrd),
                "append": self.append,
                "drives": self.drives,
                "cdroms": self.cdroms,
                "args": self.args,
            })
