#!/usr/bin/env python

from execvm import vector


class Mknod(vector.Vector):

    yaml_tag = u"!mknod"

    def __init__(self, path, device, _scall="/usr/bin/scall"):
        super().__init__()
        self.path = path
        self.device = device
        self._scall = _scall

    def vectorize(self):
        return [
            self._scall,
            "mknod",
            self.path,
            *self.device,
        ]
