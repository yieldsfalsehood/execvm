#!/usr/bin/env python

from execvm import vector

from . import cmdlang


class Dup2(vector.Vector):

    yaml_tag = u"!dup2"

    def __init__(self, oldfd, newfd, argv, _scall="/usr/bin/scall"):
        super().__init__()
        self.oldfd = oldfd
        self.newfd = newfd
        self.argv = argv
        self._scall = _scall

    def vectorize(self):

        argv = list(self.argv)

        with cmdlang.distinct_from(argv) as delimeter:
            return [
                self._scall,
                "dup2",
                *self.oldfd,
                *self.newfd,
                delimeter, *argv, delimeter,
            ]
