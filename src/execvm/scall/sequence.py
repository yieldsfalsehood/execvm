#!/usr/bin/env python

from execvm import vector

from . import cmdlang


class Sequence(vector.Vector):

    yaml_tag = u"!sequence"

    def __init__(self, argvs, _scall="/usr/bin/scall"):
        super().__init__()
        self.argvs = argvs
        self._scall = _scall

    def vectorize(self):
        return cmdlang.concat(self._scall, "sequence", self.argvs)
