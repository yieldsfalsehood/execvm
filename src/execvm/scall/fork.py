#!/usr/bin/env python

from execvm import vector

from . import cmdlang


class Fork(vector.Vector):

    yaml_tag = u"!fork"

    def __init__(self, argvs, _scall="/usr/bin/scall"):
        super().__init__()
        self.argvs = argvs
        self._scall = _scall

    def vectorize(self):
        return cmdlang.concat(self._scall, "fork", self.argvs)
