#!/usr/bin/env python3

from execvm import vector


class Open(vector.Vector):

    yaml_tag = u"!open"

    def __init__(self, method, path):
        super().__init__()
        self.method = method
        self.path = path

    def vectorize(self):
        return ["open", self.method, self.path]


class Fd(vector.Vector):

    yaml_tag = u"!fd"

    def __init__(self, handle):
        super().__init__()
        self.handle = str(handle)

    def vectorize(self):
        return ["fd", self.handle]


class Vsock(vector.Vector):

    yaml_tag = u"!vsock"

    def __init__(self, mode, addr):
        super().__init__()
        self.mode = mode
        self.addr = addr

    def vectorize(self):
        return ["vsock", self.mode, self.addr]
