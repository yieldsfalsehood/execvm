#!/usr/bin/env python3

from execvm import vector


class GPT(vector.Vector):

    yaml_tag = u"!gpt"

    def __init__(self, uuid):
        super().__init__()
        self.uuid = uuid

    def vectorize(self):
        return ["block", "gpt", self.uuid]


class GPTPartition(vector.Vector):

    yaml_tag = u"!gpt-partition"

    def __init__(self, uuid):
        super().__init__()
        self.uuid = uuid

    def vectorize(self):
        return ["block", "gpt-partition", self.uuid]


class Device(vector.Vector):

    yaml_tag = u"!dev"

    def __init__(self, path):
        super().__init__()
        self.path = path

    def vectorize(self):
        return ["dev", self.path]
