#!/usr/bin/env python3

import contextlib
import functools
import itertools


class DelimitingError(Exception):
    pass


def notin(proposed, argv):

    if proposed in argv:
        raise DelimitingError()

    return proposed


@contextlib.contextmanager
def distinct_from(argv):
    for count in itertools.count(1):
        try:
            yield notin("-"*count, argv)
        except DelimitingError:
            pass
        else:
            return


def concat2(_scall, cmd, right, left):
    # we're really doing a foldr so i'm pretending the args are
    # reversed
    lleft = list(left)
    lright = list(right)
    with distinct_from(lleft+lright) as delimeter:
        return [
            _scall,
            cmd,
            delimeter,
            *lleft,
            delimeter,
            *lright,
            delimeter,
        ]


def concat(_scall, cmd, argvs):
    # concat2 concats two argvs, so we foldr that over all the
    # argvs to concatenate them
    func = functools.partial(concat2, _scall, cmd)
    return functools.reduce(func, reversed(argvs))
