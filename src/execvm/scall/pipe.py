#!/usr/bin/env python

from execvm import vector

from . import cmdlang


class Pipe(vector.Vector):

    yaml_tag = u"!pipe"

    def __init__(self, argv, _scall="/usr/bin/scall"):
        super().__init__()
        self.argv = argv
        self._scall = _scall

    def vectorize(self):

        argv = list(self.argv)

        with cmdlang.distinct_from(argv) as delimeter:
            return [
                self._scall,
                "pipe",
                delimeter, *argv, delimeter,
            ]
