#!/usr/bin/env python3

from .close import Close

from .dup2 import Dup2

from .fork import Fork

from .mknod import Mknod

from .pipe import Pipe

from .sequence import Sequence

from . import cmdlang

concat = cmdlang.argv.concat
distinct_from = cmdlang.argv.distinct_from
Device = cmdlang.device.Device
GPT = cmdlang.device.GPT
GPTPartition = cmdlang.device.GPTPartition
Fd = cmdlang.fd.Fd
Open = cmdlang.fd.Open

__all__ = [
    "Close",
    "Dup2",
    "Fork",
    "Mknod",
    "Pipe",
    "Sequence",
    "concat",
    "distinct_from",
    "Device",
    "GPT",
    "GPTPartition",
    "Fd",
    "Open",
]
