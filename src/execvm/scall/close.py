#!/usr/bin/env python

from execvm import vector

from . import cmdlang


class Close(vector.Vector):

    yaml_tag = u"!close"

    def __init__(self, fd, argv, _scall="/usr/bin/scall"):
        super().__init__()
        self.fd = fd
        self.argv = argv
        self._scall = _scall

    def vectorize(self):

        argv = list(self.argv)

        with cmdlang.distinct_from(argv) as delimeter:
            return [
                self._scall,
                "close",
                *self.fd,
                delimeter, *argv, delimeter,
            ]
