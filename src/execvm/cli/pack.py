#!/usr/bin/env python3

import logging
import struct
import sys

from cliff.command import Command


FORMATTERS = {
    "x": lambda _: None,
    "c": lambda x: x.encode(),
    "b": int,
    "B": int,
    "?": bool,
    "h": int,
    "H": int,
    "i": int,
    "I": int,
    "l": int,
    "L": int,
    "q": int,
    "Q": int,
    "n": int,
    "N": int,
    "e": float,
    "f": float,
    "d": float,
    "s": lambda x: x.encode(),
    "p": lambda x: x.encode(),
    "P": int,
}


# https://stackoverflow.com/q/4628290
def pairs(t):
    """[t1, t2, ...] -> [(t1, t2), ...]"""
    return zip(*[iter(t)]*2)


def reformat(fmt, arg):
    return FORMATTERS[fmt[-1]](arg)


def pack(order, argv):

    fmt = order + "".join(argv[::2])

    args = filter(lambda x: x is not None,
                  map(lambda x: reformat(*x),
                      pairs(argv)))

    return struct.pack(fmt, *args)


class Pack(Command):
    "pack binary data"

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):

        parser = super().get_parser(prog_name)

        parser.add_argument("-o", default="", dest="order",
                            help="byte order spec")
        parser.add_argument("argv", nargs="+",
                            help="fmt1 arg1 fmt2 arg2 ...")

        return parser

    def take_action(self, args):
        sys.stdout.buffer.write(
            pack(args.order, args.argv))
