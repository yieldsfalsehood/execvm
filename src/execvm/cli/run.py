#!/usr/bin/env python3

import argparse
import asyncio
import logging

from cliff.command import Command
import dotenv

import execvm

from execvm import initos
from execvm import serial

from . import config
from . import utils


def convertenviron(env):
    # posixmodule's convertenviron is statically embedded there so
    # here it is again :(
    return dict(
        setting.split("=", 1)
        for setting in env
        if "=" in setting
    )


def run(env, execution, conf,
        drives, cdroms,
        kernel_log, kernel_debug, qemu_args, _qemu):

    modloop_device = "/dev/vda"
    rootfs_device = "/dev/vdb"

    qemu = initos.build_qemu(env,
                             conf.initos_magic,
                             conf.stdio_block_size,
                             conf.cache,
                             conf.kernel,
                             conf.rootfs,
                             rootfs_device,
                             drives, cdroms,
                             kernel_log, kernel_debug,
                             qemu_args,
                             _qemu)

    init = initos.build_init(modloop_device,
                             conf.stdio_block_size,
                             execution)
    if conf.dry_run:
        print(serial.dump({
            "execution": execution,
            "config": conf,
            "qemu": qemu,
        }))
    else:
        routine = initos.subprocess.run(qemu, init,
                                        conf.initos_magic)
        asyncio.run(routine)


class Run(Command):
    "run a command in an initos guest"

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):

        parser = super().get_parser(prog_name)

        parser.add_argument("--initos-magic",
                            default=initos.defaults.INITOS_MAGIC)
        parser.add_argument("--stdio-block-size",
                            default=initos.defaults.STDIO_BLOCK_SIZE)

        parser.add_argument("--kernel",
                            default=initos.defaults.KERNEL)
        parser.add_argument("--kernel-version",
                            default=initos.defaults.KERNEL_VERSION)
        parser.add_argument("--kernel-arch",
                            default=initos.defaults.KERNEL_ARCH)

        parser.add_argument("--rootfs",
                            default=initos.defaults.ROOTFS)
        parser.add_argument("--rootfs-version",
                            default=initos.defaults.ROOTFS_VERSION)
        parser.add_argument("--rootfs-arch",
                            default=initos.defaults.ROOTFS_ARCH)

        parser.add_argument("--cdrom",
                            action="append", dest="cdroms")
        parser.add_argument("--drive",
                            action="append", dest="drives")

        parser.add_argument("--cache",
                            default=execvm.defaults.CACHE)

        parser.add_argument("--qemu",
                            default=execvm.defaults.QEMU)

        group = parser.add_mutually_exclusive_group()
        group.add_argument("-c")
        group.add_argument("-m")

        parser.add_argument("-e", "--env",
                            action="append")

        parser.add_argument("--kernel-debug",
                            action="store_true")
        parser.add_argument("--kernel-log",
                            default="file:/dev/null")
        parser.add_argument("--qemu-arg",
                            action="append",
                            dest="qemu_args")

        parser.add_argument("-n", "--dry-run")

        parser.add_argument("argv", nargs=argparse.REMAINDER)

        return parser

    def take_action(self, args):

        dotenv.load_dotenv(".env")

        env = dict(**initos.defaults.ENV,
                   **convertenviron(args.env or []))

        execution = utils.parse_execution(args)

        conf = config.Config(args)

        run(
            env, execution, conf,
            drives=args.drives or [],
            cdroms=args.cdroms or [],
            qemu_args=args.qemu_args or [],
            kernel_log=args.kernel_log,
            kernel_debug=args.kernel_debug,
            _qemu=args.qemu,
        )
