#!/usr/bin/env python3

import argparse
import logging

from cliff.command import Command

from . import utils


class Exec(Command):
    "run an execution"

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):

        parser = super().get_parser(prog_name)

        group = parser.add_mutually_exclusive_group()
        group.add_argument("-c")
        group.add_argument("-m")

        parser.add_argument("argv", nargs=argparse.REMAINDER)

        return parser

    def take_action(self, args):
        execution = utils.parse_execution(args)
        execution.run()
