#!/usr/bin/env python3

import base64
import importlib
import sys

from execvm import executions
from execvm import serial


def b64decode(string):
    return base64.standard_b64decode(string.encode()).decode()


def shift(argv):
    if argv and argv[0] == "--":
        return argv[1:]
    return argv


def execution(argv):
    return executions.ExecVP(argv)


def module_execution(m, argv):
    module = importlib.import_module(m)
    return module.execution(argv)


def parse_execution(args):

    argv = shift(args.argv)

    if args.c:
        return serial.load(b64decode(args.c))
    elif args.m:
        return module_execution(args.m, argv)
    elif argv:
        return execution(argv)
    else:
        return serial.load(sys.stdin)
