#!/usr/bin/env python3

import sys

from cliff.app import App as CliffApp
from cliff.commandmanager import CommandManager


class App(CliffApp):
    def __init__(self):
        super().__init__(
            description="execvm",
            version="1.0",
            command_manager=CommandManager("cliff.execvm"),
            deferred_help=True,
        )


def main():
    app = App()
    return app.run(sys.argv[1:])
