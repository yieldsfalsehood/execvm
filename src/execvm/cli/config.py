#!/usr/bin/env python3

import os
import pathlib

from execvm import initos
from execvm import yaml


INITOS_MAGIC = "INITOS_MAGIC"
INITOS_STDIO_BLOCK_SIZE = "INITOS_STDIO_BLOCK_SIZE"
INITOS_KERNEL = "INITOS_KERNEL"
INITOS_KERNEL_VERSION = "INITOS_KERNEL_VERSION"
INITOS_KERNEL_ARCH = "INITOS_KERNEL_ARCH"
INITOS_ROOTFS = "INITOS_ROOTFS"
INITOS_ROOTFS_VERSION = "INITOS_ROOTFS_VERSION"
INITOS_ROOTFS_ARCH = "INITOS_ROOTFS_ARCH"

EXECVM_CACHE = "EXECVM_CACHE"


def load_config(key, default):
    return os.environ.get(key, default)


class Config(yaml.YAMLObject):

    yaml_tag = "!config"

    def __init__(self, args):

        super().__init__()

        self.initos_magic = load_config(INITOS_MAGIC,
                                        args.initos_magic)
        self.stdio_block_size = load_config(INITOS_STDIO_BLOCK_SIZE,
                                            args.stdio_block_size)

        self.kernel = initos.artifacts.Kernel(
            load_config(INITOS_KERNEL,
                        args.kernel),
            load_config(INITOS_KERNEL_VERSION,
                        args.kernel_version),
            load_config(INITOS_KERNEL_ARCH,
                        args.kernel_arch)
        )

        self.rootfs = initos.artifacts.RootFS(
            load_config(INITOS_ROOTFS,
                        args.rootfs),
            load_config(INITOS_ROOTFS_VERSION,
                        args.rootfs_version),
            load_config(INITOS_ROOTFS_ARCH,
                        args.rootfs_arch)
        )

        self.cache = pathlib.Path(load_config(EXECVM_CACHE,
                                              args.cache))

        self.dry_run = args.dry_run

    @classmethod
    def to_yaml(cls, dumper, self):
        return dumper.represent_mapping(
            cls.yaml_tag,
            {
                "initos-magic": self.initos_magic,
                "stdio-block-size": self.stdio_block_size,
                "kernel": self.kernel,
                "rootfs": self.rootfs,
                "cache": str(self.cache),
            })
