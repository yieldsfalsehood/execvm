#!/usr/bin/env python3

import asyncio
import logging
import sys

from cliff.command import Command

from execvm.initos import blockstream


class EncodeStream(Command):
    "encode stream"

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):

        parser = super().get_parser(prog_name)

        parser.add_argument("block_size", type=int)

        return parser

    def take_action(self, args):
        asyncio.run(
            encode(args.block_size, sys.stdin, sys.stdout.buffer))


class DecodeStream(Command):
    "decode stream"

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):

        parser = super().get_parser(prog_name)

        parser.add_argument("block_size", type=int)

        return parser

    def take_action(self, args):
        asyncio.run(
            decode(args.block_size, sys.stdin, sys.stdout.buffer))


async def encode(block_size, fin, fout):
    eof = asyncio.get_event_loop().create_future()
    await blockstream.encode(block_size, fin, fout, eof)
    await eof


async def decode(block_size, fin, fout):
    eof = asyncio.get_event_loop().create_future()
    await blockstream.decode(block_size, fin, fout, eof)
    await eof
