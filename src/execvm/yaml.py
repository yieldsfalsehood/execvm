#!/usr/bin/env python3

import yaml


class YAMLObject(yaml.YAMLObject):

    yaml_loader = yaml.SafeLoader
    yaml_dumper = yaml.SafeDumper

    def __setstate__(self, state):
        self.__init__(**state)
