#!/usr/bin/env python3

from . import vector


class Exec(vector.Vector):

    yaml_tag = u"!exec"

    def __init__(self, c=None, m=None, argv=None,
                 _execvm="/usr/bin/execvm"):
        super().__init__()
        self.c = c
        self.m = m
        self.argv = argv or []
        self._execvm = _execvm

    def vectorize(self):
        if self.c:
            return [
                self._execvm,
                "exec", "-c", str(self.c),
            ]
        elif self.m:
            return [
                self._execvm,
                "exec", "-m", str(self.m),
                *self.argv,
            ]
        elif self.argv:
            return [self._execvm, "exec", *self.argv]
        else:
            return [self._execvm, "exec"]
