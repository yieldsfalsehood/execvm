#!/usr/bin/env python3

# this is separate from the yaml module so we can import all the yaml
# objects here before calling load without creating an import loop

import pathlib

import yaml

from execvm import exec  # noqa: F401
from execvm import executions  # noqa: F401
from execvm import initos  # noqa: F401
from execvm import qemu  # noqa: F401
from execvm import scall  # noqa: F401


def load(stream):
    return yaml.safe_load(stream)


def dump(data, stream=None, **kwargs):
    return yaml.safe_dump(data, stream,
                          explicit_start=True,
                          **kwargs)


def path_representer(dumper, path):
    return dumper.represent_scalar("!!str", str(path))


yaml.add_multi_representer(pathlib.Path, path_representer)
