#!/usr/bin/env python3

import itertools

from . import yaml


class Vector(yaml.YAMLObject):

    def vectorize(self):
        raise NotImplementedError()

    # def __str__(self):
    #     return yaml.dump(self.vectorize())

    def __iter__(self):
        return iter(self.vectorize())

    def __radd__(self, itr):
        return itertools.chain(itr, self)
