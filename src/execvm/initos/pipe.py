#!/usr/bin/env python3

import os
import struct


class Fd:

    def __init__(self, fd):
        self._fd = fd

    def close(self):
        return os.close(self._fd)

    def fileno(self):
        return self._fd

    def read(self, length):

        buf = os.read(self._fd, length)
        size = len(buf)

        if size == length:
            return buf

        return buf + self.read(length-size)

    def uint16(self):
        buf = self.read(2)
        value, = struct.unpack(">H", buf)
        return value


class Pipe:
    def __init__(self):
        read, write = os.pipe()
        self.read = Fd(read)
        self.write = Fd(write)
