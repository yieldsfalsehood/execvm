#!/usr/bin/env python3

# uint16
INITOS_MAGIC = "1234"
STDIO_BLOCK_SIZE = "4096"

KERNEL = "alpine-netboot"
KERNEL_VERSION = "3.12.3"
KERNEL_ARCH = "x86_64"

ROOTFS = "initos"
ROOTFS_VERSION = "0.1.0"
ROOTFS_ARCH = "x86_64"

PATH = ":".join([
    "/usr/local/sbin",
    "/usr/local/bin",
    "/usr/sbin",
    "/usr/bin",
    "/sbin",
    "/bin",
])

ENV = {
    "PATH": PATH,
}
