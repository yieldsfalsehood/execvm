#!/usr/bin/env python3

from execvm import exec
from execvm import executions
from execvm import qemu
from execvm import scall

from . import blockstream

KERNEL_CONSOLE = "/dev/ttyS0"
DATA_CONSOLE = "/dev/ttyS1"


def build_bootstrap(env, initos_magic, stdio_block_size):
    return executions.ExecVE(
            env=env,
            argv=scall.Dup2(
                scall.Open("write", DATA_CONSOLE), scall.Fd("1"),
                scall.Dup2(
                    scall.Open("read", DATA_CONSOLE), scall.Fd("0"),
                    scall.Sequence([
                        ["/bin/stty", "raw", "-echo"],
                        ["/bin/stty", "-F", KERNEL_CONSOLE, "raw", "-echo"],
                        ["/usr/bin/execvm", "pack",
                         "-o", ">", "H", initos_magic],
                        ["/usr/bin/execvm", "pack",
                         "-o", ">", "H", stdio_block_size],
                        scall.Pipe(
                            scall.Fork([
                                scall.Close(
                                    scall.Fd("5"),
                                    scall.Sequence([
                                        scall.Dup2(
                                            scall.Fd("6"), scall.Fd("1"),
                                            blockstream.Decode(stdio_block_size),
                                        ),
                                        scall.Close(scall.Fd("6"),
                                                    ["/bin/true"]),
                                    ])),
                                scall.Close(
                                    scall.Fd("6"),
                                    scall.Dup2(
                                        scall.Fd("5"), scall.Fd("0"),
                                        exec.Exec()))]))]))))


def build_qemu(env, initos_magic, stdio_block_size,
               cache, kernel, rootfs, rootfs_device,
               drives, cdroms,
               kernel_log, kernel_debug, qemu_args, _qemu):

    modloop = cache/str(kernel)/"modloop-virt"
    root = cache/str(rootfs)

    initos_drives = [
        f"file={modloop},format=raw,if=virtio",
        f"file={root},format=raw,if=virtio",
    ]

    initos_args = [
        "-machine", "type=pc,accel=kvm",
        "-m", "512M",
        "-nographic",
        "-no-reboot",
        "-monitor", "/dev/null",
        "-serial", kernel_log,
        "-serial", "stdio",
        "-nic", "none",
    ]

    bootstrap = build_bootstrap(env, initos_magic, stdio_block_size)

    return qemu.Qemu(
        cache/str(kernel)/"vmlinuz-virt",
        cache/str(kernel)/"initramfs-virt",
        qemu.AlpineCommandLine(KERNEL_CONSOLE,
                               rootfs_device,
                               bootstrap,
                               debug=kernel_debug),
        drives=initos_drives+drives,
        cdroms=cdroms,
        args=initos_args+qemu_args,
        _qemu=_qemu,
    )


def build_init(modloop_device, stdio_block_size, execution):
    return executions.ExecV(
        argv=scall.Dup2(
            scall.Open("write", KERNEL_CONSOLE), scall.Fd("2"),
            # stdin got clobbered to receive this program
            # description. rather than save a copy of the original fd
            # and reset that here, i just create a new one opened to
            # the same terminal. nobody has visibility to the original
            # fd, so we should be safe from any unexpected io from
            # processes that aren't us.
            scall.Dup2(
                scall.Open("read", DATA_CONSOLE), scall.Fd("0"),
                scall.Sequence([
                    ["/bin/mount", modloop_device, "/.modloop"],
                    ["/bin/mount", "--bind",
                     "/.modloop/modules", "/lib/modules"],
                    ["/bin/busybox", "modprobe", "-a",
                     "sr_mod",
                     "sd_mod",
                     ],
                    ["/usr/bin/execvm", "pack",
                     "-o", ">", "H", stdio_block_size],
                    scall.Pipe(
                        scall.Fork([
                            scall.Close(
                                scall.Fd("8"),
                                scall.Sequence([
                                    scall.Dup2(
                                        scall.Fd("9"), scall.Fd("1"),
                                        blockstream.Decode(stdio_block_size)),
                                    scall.Close(scall.Fd("9"),
                                                ["/bin/true"]),
                                ])),
                            scall.Close(
                                scall.Fd("9"),
                                scall.Dup2(
                                    scall.Fd("8"), scall.Fd("0"),
                                    scall.Sequence([
                                        exec.Exec(c=execution),
                                        ["/sbin/poweroff", "-f"],
                                    ])))]))]))))
