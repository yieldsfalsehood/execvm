#!/usr/bin/env python3

import asyncio
import functools


class ForwarderProtocol(asyncio.Protocol):

    def __init__(self, fout):
        self.fout = fout

    def connection_lost(self, exc):
        # this triggers when our stdout is closed, so we can't write
        # to it anymore
        # print("FUCK", exc)
        pass

    def data_received(self, data):
        self.fout.write(data)
        self.fout.flush()

    def eof_received(self):
        self.fout.flush()
        self.fout.close()


async def forward(fin, fout):
    await asyncio.get_event_loop().connect_read_pipe(
        functools.partial(ForwarderProtocol, fout),
        fin)
