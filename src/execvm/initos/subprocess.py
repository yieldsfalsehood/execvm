#!/usr/bin/env python3

import asyncio
import sys

from execvm import serial

from . import blockstream
from . import pipe
from . import forwarder


async def run(qemu, init, initos_magic):

    stdout = pipe.Pipe()

    print("starting emulator", file=sys.stderr)

    proc = await asyncio.create_subprocess_exec(
        *qemu,
        stdin=asyncio.subprocess.PIPE,
        stdout=stdout.write,
        stderr=None,
    )

    print("waiting for magic", file=sys.stderr)
    guest_magic = stdout.read.uint16()
    # assert guest_magic == initos_magic
    print("magic", guest_magic, initos_magic, file=sys.stderr)

    # vt junk. i thought i could avoid this for longer, but migrating
    # exec to python forced the issue. there are two problems: 1)
    # isatty returns true for everybody; 2) i can't seem to get people
    # to ignore that. if i run python -I for instance, he stops
    # sending the terminal setting codes but he does still write the
    # bytes "0m" somewhere at exit. rather than try to fight that
    # right now, i'm skipping over it. in the near future i think
    # initos should provide a preload shim that implements an isatty
    # that always returns false. then we can use env to hoist that
    # into whatever execution paths we want.
    stdout.read.read(4)

    print("waiting for block size", file=sys.stderr)
    block_size = stdout.read.uint16()
    # vt junk
    stdout.read.read(4)
    print("block size", block_size, file=sys.stderr)

    program = serial.dump(init, explicit_end=True).encode()
    print("sending program", file=sys.stderr)
    # print(program)
    encoder = blockstream.EncodeProtocol(block_size, proc.stdin,
                                         close=False)
    encoder.data_received(program)
    encoder.eof_received()

    print("waiting for block size 2", file=sys.stderr)
    block_size = stdout.read.uint16()
    # vt junk
    stdout.read.read(4)
    print("block size 2", block_size, file=sys.stderr)

    print("forwarding stdout", file=sys.stderr)
    await forwarder.forward(stdout.read, sys.stdout.buffer)

    print("encoding stdin", file=sys.stderr)
    eof = asyncio.get_event_loop().create_future()
    await blockstream.encode(block_size, sys.stdin, proc.stdin, eof)

    print("waiting for the emulator to finish", file=sys.stderr)
    status = await proc.wait()

    print("emulator exited status", status, file=sys.stderr)

    return status
