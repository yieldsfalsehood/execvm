#!/usr/bin/env python3

import asyncio
import functools
import re


# 7-bit and 8-bit C1 ANSI sequences
ANSI_ESCAPE_8BIT = re.compile(br"""
    (?: # either 7-bit C1, two bytes, ESC Fe (omitting CSI)
        \x1B
        [-_@-Z]
    |   # or a single 8-bit byte Fe (omitting CSI)
        [\x80-\x9A\x9C-\x9F]
    |   # or CSI + control codes
        (?: # 7-bit CSI, ESC [
            \x1B\[
        |   # 8-bit CSI, 9B
            \x9B
        )
        [0-?]*  # Parameter bytes
        [ -/]*  # Intermediate bytes
        [@-~]   # Final byte
    |   \x1B [clmnop]
    )
""", re.VERBOSE)


class VTScrubProtocol(asyncio.Protocol):

    def __init__(self, fout):
        self.fout = fout

    def data_received(self, data):
        stripped = ANSI_ESCAPE_8BIT.sub(b"", data)
        self.fout.write(stripped)
        self.fout.flush()

    def eof_received(self):
        self.fout.flush()
        self.fout.close()


async def vtscrub(fin, fout):
    await asyncio.get_event_loop().connect_read_pipe(
        functools.partial(VTScrubProtocol, fout),
        fin)
