#!/usr/bin/env python3

import asyncio
import collections
import functools
import struct

from execvm import vector


def slice(data, capacity, index):
    """slice and pack up to capacity bytes"""

    offset = capacity*index
    payload = data[offset:offset+capacity]
    size = len(payload)

    return struct.pack(">H", size) + payload


def pad(size, data):
    # assert len(data) < size
    yield from data
    yield from bytes(size - len(data))


def subsequences(data, size):
    return map(lambda offset: data[offset:offset+size],
               map(lambda index: size*index,
                   range(-(-len(data)//size))))


class EncodeProtocol(asyncio.Protocol):

    def __init__(self, block_size, fout, close=True, eof=None):
        self.block_size = block_size
        self.fout = fout
        self.close = close
        self.eof = eof

    def pad(self, data):
        return bytes(pad(self.block_size, data))

    def forward(self, block):
        # assert len(block) < self.block_size
        packet = self.pad(b"\x30" + block)
        self.fout.write(packet)

    def blocks(self, data):

        header_size = 3
        payload_capacity = self.block_size - header_size

        return map(
            functools.partial(slice, data, payload_capacity),
            range(-(-len(data)//payload_capacity))
        )

    def data_received(self, data):
        for block in self.blocks(data):
            self.forward(block)

    def eof_received(self):

        packet = self.pad(b"\x31" + b"")
        self.fout.write(packet)

        if self.close:
            self.fout.close()

        if self.eof:
            self.eof.set_result(None)


class DecodeProtocol(asyncio.Protocol):

    def __init__(self, block_size, fout, eof=None):

        self.block_size = block_size
        self.fout = fout
        self.eof = eof

        # please switch to protobuf or such
        self.buff = collections.deque([b""])
        header_format = ">BH"
        payload_size = block_size - struct.calcsize(header_format)
        self.format = struct.Struct(header_format
                                    + str(payload_size) + "s")

    def consume_buff(self):
        if self.buff:
            return self.buff.popleft() + self.consume_buff()
        return b""

    def complete(self, block):
        return len(block) == self.block_size

    def incomplete(self, block):
        return not self.complete(block)

    def parse(self, data):
        blocks = list(subsequences(data, self.block_size))
        return (filter(self.complete, blocks),
                b"".join(filter(self.incomplete, blocks)))

    def data_handler(self, size, payload):
        self.fout.write(payload[:size])

    def eof_handler(self, _, __):
        if self.eof:
            self.eof.set_result(None)

    def dispatch(self, block):

        command, arg, payload = self.format.unpack(block)

        handlers = {
            0x30: self.data_handler,
            0x31: self.eof_handler,
        }

        handler = handlers[command]

        handler(arg, payload)

    def run(self):

        consumed = self.consume_buff()

        blocks, unconsumed = self.parse(consumed)

        for block in blocks:
            self.dispatch(block)

        self.buff.append(unconsumed)

    def data_received(self, data):
        self.buff.append(data)
        self.run()

    def eof_received(self):
        if self.eof:
            self.eof.set_result(None)


class Decode(vector.Vector):

    yaml_tag = u"!decode-stream"

    def __init__(self, block_size, _execvm="/usr/bin/execvm"):
        super().__init__()
        self.block_size = block_size
        self._execvm = _execvm

    def vectorize(self):

        return [
            self._execvm,
            "decode-stream",
            str(self.block_size)
        ]


async def encode(block_size, fin, fout, eof=None):
    await asyncio.get_event_loop().connect_read_pipe(
        functools.partial(EncodeProtocol, block_size, fout, eof),
        fin)


async def decode(block_size, fin, fout, eof=None):
    await asyncio.get_event_loop().connect_read_pipe(
        functools.partial(DecodeProtocol, block_size, fout, eof),
        fin)
