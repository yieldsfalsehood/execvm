#!/usr/bin/env python3

from execvm import yaml

from . import defaults


class Artifact(yaml.YAMLObject):

    def __init__(self, name, version, arch):
        super().__init__()
        self.name = name
        self.version = version
        self.arch = arch


class Kernel(Artifact):

    yaml_tag = u"!kernel"

    def __init__(self,
                 name=defaults.KERNEL,
                 version=defaults.KERNEL_VERSION,
                 arch=defaults.KERNEL_ARCH):
        super().__init__(name, version, arch)

    def __str__(self):
        return f"{self.name}-{self.version}-{self.arch}"


class RootFS(Artifact):

    yaml_tag = u"!rootfs"

    def __init__(self,
                 name=defaults.ROOTFS,
                 version=defaults.ROOTFS_VERSION,
                 arch=defaults.ROOTFS_ARCH):
        super().__init__(name, version, arch)

    def __str__(self):
        return f"{self.name}-rootfs-{self.version}-{self.arch}.squashfs"
