#!/usr/bin/env python3

import base64
import os

from . import yaml


def b64encode(string):
    return base64.standard_b64encode(string.encode()).decode()


class Execution(yaml.YAMLObject):

    def run(self):
        raise NotImplementedError()

    def __str__(self):
        return b64encode(yaml.yaml.safe_dump(self, default_flow_style=True))


class ExecV(Execution):

    yaml_tag = u"!execv"

    def __init__(self, argv):
        # argv: Itr[Str]
        super().__init__()
        self.argv = argv

    def run(self):
        argv = list(self.argv)
        os.execv(argv[0], argv)


class ExecVE(Execution):

    yaml_tag = u"!execve"

    def __init__(self, argv, env):
        # argv: Itr[Str]
        # env: Map[Str, Str]
        super().__init__()
        self.argv = argv
        self.env = env

    def run(self):
        argv = list(self.argv)
        os.execve(argv[0], argv, self.env)


class ExecVP(Execution):

    yaml_tag = u"!execvp"

    def __init__(self, argv):
        # argv: Itr[Str]
        super().__init__()
        self.argv = argv

    def run(self):
        argv = list(self.argv)
        os.execvp(argv[0], argv)
