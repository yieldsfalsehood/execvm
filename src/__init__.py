#!/usr/bin/env python3

import os


def version():
    return os.environ["VERSION"]
