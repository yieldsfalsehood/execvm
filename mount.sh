
# sr_mod

# mount with minirootfs and extended iso. standard iso doesn't have
# all the pkgs we want.

execvm run --kernel-output=kernel.log \
  --drive "media=cdrom,index=2,file=$HOME/tmp/alpine-minirootfs-3.12.3-x86_64.iso" \
  --drive "media=cdrom,index=3,file=$HOME/tmp/alpine-extended-3.12.3-x86_64.iso" \
  /usr/bin/sequence \
    - \
    /bin/mount -t iso9660 -o ro /dev/sr0 /mnt \
    - \
    /usr/bin/sequence \
      -- \
      /bin/busybox lsmod \
      -- \
      /usr/bin/sequence \
        --- \
        /bin/mount -t iso9660 -o ro /dev/sr1 /mnt/opt \
        --- \
        /usr/bin/sequence \
          ---- \
          /bin/mount -t tmpfs tmpfs /mnt/mnt \
          ---- \
          /usr/bin/sequence \
            ----- \
            /bin/echo here we goo \
            ----- \
            /usr/sbin/chroot /mnt \
              /sbin/apk add \
                --repository file:///opt/apks \
                --keys-dir /etc/apk/keys \
                --root /mnt \
                --update-cache \
                --initdb \
                alpine-base \
                alpine-baselayout \
                busybox \
                busybox-suid \
                dosfstools \
                e2fsprogs \
                parted \
                xfsprogs \
            ----- \
          ---- \
        --- \
      -- \
    -
