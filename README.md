
# execvm

`execve` for virtual\* machines

## Installation

```sh
$ make develop
$ make install
```

## Overview

This package defines a DSL for declaring linux process executions and
implements tools to perform those executions in the current kernel
(`execvm exec`) or in a guest kernel (`execvm run`).

```sh
$ execvm exec /bin/echo hello world
$ execvm run /bin/echo hello world
```

A description of the protocol used to orchestrate performing the
execution virtualized is provided at the end of this document.

## Commands

### exec

```sh
$ execvm exec < yaml
$ execvm exec argv+
$ execvm exec -c b64yaml
$ execvm exec -m module argv*
```

### run

```sh
$ execvm run < yaml
$ execvm run argv+
$ execvm run -c b64yaml
$ execvm run -m module argv*
```

### pack

```
$ execvm pack fmt1 arg1 fmt2 arg2 ...
```

### stream encoding

#### encode-stream

#### decode-stream

## YAML Schema

This doesn't conform to any extant yaml schema language, but it's
meant to look like a regular grammar in a yamly format. Lists are used
for options. The extra underscores are there to make this a correct
document so the syntax highlighting looks reasonable.

```yaml
!fd
handle: !!string _

!open
method: ["read", "write"]
path: !!string _

!vsock
mode: ["listen", "connect"]
addr: !!string _

fd:
  - !fd _
  - !open _
  - !vsock _
  - [!!string _]

!gpt
uuid: !!string _

!gpt-partition
uuid: !!string _

!dev
path: !!string _

device:
  - !gpt _
  - !gpt-partition _
  - !dev _
  - [!!string _]

!execv
argv: argv

!execve
argv: argv
env: !!map { !!str _: !!str _ }

!execvp
argv: argv

execution:
  - !execv _
  - !execve _
  - !execvp _

!close
fd: fd
argv: argv
_scall: !!string _

!decode-stream
block_size: !!string _
fin: fd
fout: fd
_scall: !!string _

!dup2
oldfd: fd
newfd: fd
argv: argv
_scall: !!string _

!exec
c: [!!string _, !!null _]
m: [!!string _, !!null _]
argv: argv
_execvm: !!string _

!fork
argvs: [argv]
_scall: !!string _

!mknod
path: !!string _
device: device
_scall: !!string _

!pipe
argv: argv
_scall: !!string _

!alpine-command-line
console: !!string _
root: !!string _
execution: execution
init: !!string _
debug: !!bool _

kernel-command-line:
  - !alpine-command-line _
  - !!string _

!qemu
kernel: !!string _
initrd: !!string _
append: kernel-command-line
drives: [!!string _]
cdroms: [!!string _]
args: [!!string _]
_qemu: !!string _

!sequence
argvs: [argv]
_scall: !!string _

argv:
  - !close _
  - !decode-stream _
  - !dup2 _
  - !exec _
  - !fork _
  - !mknod _
  - !pipe _
  - !qemu _
  - !sequence _
  - [!!string _]

!kernel
name: !!string _
version: !!string _
arch: !!string _

!rootfs
name: !!string _
version: !!string _
arch: !!string _

!config
initos_magic: !!string _
stdio_block_size: !!string _
kernel: !kernel _
rootfs: !rootfs _
cache: !!string _
dry_run: !!bool _
```

## Python API

In lieu of documentation for the Python API:

the `cli` subpackage implements a handful of utilities through a
single binary `cliff` app deferring to the libraries defined in the
rest of the package.

The `scall` subpackage abstracts the tools from `scall`:

```
scall.Dup2(
    scall.Open("write", "/dev/null),
    scall.Fd("1"),
    argv=["/bin/echo", "foo"])
```

## initos

This package defines a synchronizing protocol to communicate with the
guest and pass the intended execution. For the sake of getting things
written down for now:

I have a pre-defined "bootstrap" execution in the code here
(`execvm.initos.initos`) that is an `ExecVE` that configures the data
terminal for raw mode, writes a magic number to stdout, then calls
`execvm exec` with no arguments. That form causes the process to read
a new execution from stdin then execute that. In the same module I
define an "init" execution. This is what gets fed to the bootstrap
execution's stdin, and is free to be as long as complex as
desired. This should cover what's typically in an `/sbin/init`. To
that end I mount the modloop image and load some standard modules. As
part of IO synchronization I write my stream encoding block size to
stdout, then I pipe and fork. In one process I decode stdin to the
write end of the pipe - when I receive an encoded eof message I close
the pipe and exit.

The other process runs the passed in program, which has also been
encapsulated in an execution (with the side benefit of once again
resetting our command line argument count), via `execvm exec`. I run
this with stdin redirected from the read end of that pipe from
earlier - this way if the user application is reading from stdin then
he'll get eof as expected. Finally I do a normal shutdown after the
user process exits. I need to look into data syncing and any other
formal clean up that needs to be done here, so until then I'm not
totally confident in data consistency.

On the "client" side I start qemu then wait to read two bytes from its
stdout - this should be the magic number. I compare that for sanity
and if that's OK I wait to read another two bytes. This is the
"server" side setting the block size to use for io encoding, which has
the side effect of signaling to the client that the server is ready to
receive data. The terminal is unbuffered so if we start writing to
qemu's stdin before an application has opened the terminal in the
guest, that data would be lost. Having received this block size that
means the init execution has reached such a point, so even if the user
application isn't yet running I at least know the data will be
buffered in the right places for when it is.

Also at this point I begin simply forwarding qemu's stdout to our
stdout. I need to use this channel for those first four bytes to
synchronize, but after that I yield it entirely to the user
application.

I do some scrubbing on stderr because the sealab bios insists on
writing a boot message that includes terminal control characters. I
strip those so I can do plain `execvm run` for now, although I think
I'd like to remove that logic eventually. I don't know what api I want
around that yet, though. I had done some early work to consider
segmenting the stderr stream where I'd write some delimiter from
inside the init script and then outside I could say anything between
the delimiters is the normal application's stderr, everything else is
the bootloader and kernel. That's a little fussy though and stateful
in a way I didn't like. That brings me back to thinking the right
answer is not having an opinion about this. In the future I think I'd
like to write stderr unaltered and let the caller decide to pass
through awk or write to a file or `/dev/null`. But, I want to see how
that looks first before stranding myself at sea and until I know of a
better answer it's nice not having to redirect to a file or have my
terminal get clobbered when I forget to.
